var util = require('./util')

exports.getfavorite = getfavorite
exports.insertfavorite = insertfavorite
exports.deletefavorite = deletefavorite

function getfavorite(scode,ucode,callback) {
	util.setpool(ucode)
	var rows = new Object()
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			//util.newerror(code + '019')
			if(typeof callback ==='function') callback(0)
		} else {
			connection.query('select bname from bfavorite where scode = ?',[scode],
			function(err,bfrows) {
				if(err) {
					connection.release()
					log(err)
					//util.newerror(code + '020')
					if(typeof callback ==='function') callback(0)
				} else {
					rows.bfav = bfrows
					connection.query('select bname,rid from rfavorite where scode = ?',[scode],
					function(err,rfrows) {
						connection.release()
						if(err) {
							log(err)
							//util.newerror(code + '021')
							if(typeof callback ==='function') callback(0)
						} else {
							rows.rfav = rfrows
							if(typeof callback ==='function') callback(rows)
						}
					})
				}
			})
		}
	})
}

function insertfavorite(scode,bname,rid,ucode,callback) {
	util.setpool(ucode)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			util.newerror(code + '022')
			if(typeof callback ==='function') callback(0)
		} else {
			if(rid == null || rid == undefined) {
				connection.query('insert into bfavorite set ?',{"scode" : scode ,"bname" : bname},
				function(err,results) {
					connection.release()
					if(err){
						log(err)
						util.newerror(code + '023')
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(10) //building
					}
				})
			} else if(rid != null || rid != undefined) {
				connection.query('insert into rfavorite set ?',{"scode" : scode ,"bname" : bname,"rid" : rid},
				function(err,results) {
					connection.release()
					if(err) {
						log(err)
						util.newerror(code + '024')
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(11) //room
					}
				})
			} else {
				util.newerror(code + '025')
				if(typeof callback ==='function') callback(0)
			}
		}
	})
}

function deletefavorite(scode,bname,rid,ucode,callback) {
	util.setpool(ucode)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			//util.newerror(code + '026')
			if(typeof callback ==='function') callback(0)
		} else {
			if(rid == null || rid == undefined) {
				connection.query('delete from bfavorite where scode = ? and bname = ?',[scode,bname],
				function(err,results) {
					connection.release()
					if(err){
						log(err)
						//util.newerror(code + '027')
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(10)
					}
				})
			} else if(rid != null || rid != undefined) {
				connection.query('delete from rfavorite where scode = ? and bname = ? and rid = ?',[scode,bname,rid],
				function(err,results) {
					connection.release()
					if(err) {
						log(err)
						//util.newerror(code + '028')
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(11)
					}
				})
			} else {
				//util.newerror(code + '029')
				if(typeof callback ==='function') callback(0)
			}
		}
	})
}

function log(msg) {
	util.log(msg)
}

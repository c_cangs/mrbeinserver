const uuid = require('uuid/v4')
const nodemailer = require('nodemailer')
const util = require('./util')

const transport = nodemailer.createTransport({
  service : 'Gmail',
  auth :  {
    user : 'clabmasters@gmail.com',
    pass : 'masters@masters'
  }
})

var redisclient

exports.getredis = getredis
exports.startauth = startauth
exports.verifyauth = verifyauth
exports.test = test
exports.verifytest = verifytest

function getredis(client) {
  redisclient = client
}

function test(scode,email,callback) {
  id = uuid()
  redisclient.set(scode,id,'EX',600)
  transport.sendMail({
    from : 'clabmasters@gmail.com',
    to : email,
    subject : '미스터빈 인증 메일입니다',
    html : '<p>미스터빈 이메일 인증을 위해 아래 링크를 클릭해 주세요</p><a href=\"http://1.255.55.179:8008/authtest?id='+id+'&code='+scode+'\">링크</a><p>10분 이내에 클릭하셔야 인증이 완료됩니다</p>'
  },function(err,info) {
    if(err) {
      log(err)
      callback(0)
    } else {
      log('Email sent from ' + email + 'response : ' + info.response)
      callback(1)
    }
  })
}

function startauth(scode,email,callback) {
  util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			callback(0)
		} else {
			connection.query('select auth from clients where scode = ?',[scode],
      function(err,rows) {
        connection.release()
				if(err) {
					log(err)
					callback(0)
				} else {
					if(rows[0].auth == 1) {
						callback(2) //already
					} else {
            id = uuid()
            redisclient.set(scode,id,'EX',600)
            transport.sendMail({
              from : 'clabmasters@gmail.com',
              to : email,
              subject : '미스터빈 인증 메일입니다',
              html : '<p>미스터빈 이메일 인증을 위해 아래 링크를 클릭해 주세요</p><a href=\"http://1.255.55.179:8008/authmail?id='+id+'&code='+scode+'\">링크</a><p>10분 이내에 클릭하셔야 인증이 완료됩니다</p>'
            },function(err,info) {
              if(err) {
                log(err)
                callback(0)
              } else {
                log('Email sent from ' + email + 'response : ' + info.response)
                callback(1)
              }
            })
					}
				}
			})
		}
	})
}

function verifytest(scode,id,callback) {
  redisclient.get(scode,function(err,reply) {
    if(err) {
      log(err)
      callback(0)
    } else {
      console.log(reply)
      console.log(scode)
      console.log(id)
      if(reply == id) {
        redisclient.set(scode,0,'EX',1)
        callback(1)
      } else callback(0)
    }
  })
}

function verifyauth(scode,id,callback) {
  util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			callback(0)
		} else {
			connection.query('select auth from clients where scode = ?',[scode],
      function(err,rows) {
				if(err) {
          connection.release()
					log(err)
					callback(0)
				} else {
					if(rows[0].auth == 1) {
						callback(2) //already
					} else {
            redisclient.get(scode,function(err,reply) {
              if(err) {
                log(err)
                callback(0)
              } else {
                if(reply == id) {
                  connection.query('update clients set auth = ? where scode = ?',[1,authlist[scode]],
                  function(err,result) {
                    connection.release()
                    if(err) {
                      log(err)
                      callback(0)
                    } else {
                      redisclient.set(scode,0,'EX',1)
                      callback(1)
                    }
                  })
                } else callback(0)
              }
            })
          }
        }
      })
    }
  })
}

function log(msg) {
	util.log(msg)
}

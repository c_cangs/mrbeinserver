var util = require('./util')

exports.getdb = getdb
exports.getdbV = getdbV
exports.getdbCustom = getdbCustom
exports.getunivlist = getunivlist
exports.getdepartmentlist = getdepartmentlist

function getdb(ucode, callback) { //
  util.setpool(ucode)
  var year
  var term
  var rows = new Object()
  util.getpool(ucode).getConnection(function(err, connection) {
    if (err) {
      log(err)
      //util.newerror(code+'006')
      if (typeof callback === 'function') callback(0)
    } else {
      connection.query('select max(ver) as ver from uversion', function(err, verrow) {
        if (err) {
          connection.release()
          log(err)
          if (typeof callback === 'function') callback(0)
        } else {
          rows.ver = verrow
          connection.query('select year,term from tinfo where ver = (select max(ver) from tinfo)', function(err, trow) {
            if (err) {
              connection.release()
              log(err)
              //util.newerror(code+'007')
              if (typeof callback === 'function') callback(0)
            } else {
              rows.tinfo = trow
              year = trow[0].year
              term = trow[0].term
              connection.query('select cid,point,type,department,cname,prof,grade from class where year = ? and term = ?', [year, term],
                function(err, classrow) {
                  if (err) {
                    connection.release()
                    log(err)
                    //util.newerror(code + '008')
                    if (typeof callback === 'function') callback(0)
                  } else {
                    rows.class = classrow
                    connection.query('select bname,rid,cid,cday,speriod,eperiod from classtable where year = ? and term = ?', [year, term],
                      function(err, classtablerow) {
                        if (err) {
                          connection.release()
                          log(err)
                          //util.newerror(code + '009')
                          if (typeof callback === 'function') callback(0)
                        } else {
                          rows.classtable = classtablerow
                          connection.query('select * from building', function(err, buildingrow) {
                            if (err) {
                              connection.release()
                              log(err)
                              //util.newerror(code + '010')
                              if (typeof callback === 'function') callback(0)
                            } else {
                              rows.building = buildingrow
                              connection.query('select * from classroom', function(err, classroomrow) {
                                if (err) {
                                  connection.release()
                                  log(err)
                                  //util.newerror(code + '011')
                                  if (typeof callback === 'function') callback(0)
                                } else {
                                  rows.classroom = classroomrow
                                  connection.query('select period,stime,etime,title from schedule where year = ? and term = ?', [year, term],
                                    function(err, schedulerow) {
                                      connection.release()
                                      if (err) {
                                        log(err)
                                        //util.newerror(code + '012')
                                        if (typeof callback === 'function') callback(0)
                                      } else {
                                        rows.schedule = schedulerow
                                        if (typeof callback === 'function') callback(rows)
                                      }
                                    })
                                }
                              })
                            }
                          })
                        }
                      })
                  }
                })
            }
          })
        }
      })
    }
  })
}

function getdbV(ucode, callback) {
  util.setpool(ucode)
  util.getpool(ucode).getConnection(function(err, connection) {
    if (err) {
      log(err)
      //util.newerror(code + '013')
      if (typeof callback === 'function') callback(0)
    } else {
      connection.query('select max(ver) as ver from uversion', function(err, rows) {
        connection.release()
        if (err) {
          log(err)
          //util.newerror(code + '014')
          if (typeof callback === 'function') callback(0)
        } else {
          if (typeof callback === 'function') callback(rows[0].ver)
        }
      })
    }
  })
}

function getdbCustom(ucode, year, term, callback) {
  util.setpool(ucode)
  var rows = new Object()
  util.getpool(ucode).getConnection(function(err, connection) {
    if (err) {
      log(err)
      //util.newerror(code+'006')
      if (typeof callback === 'function') callback(0)
    } else {
      connection.query('select cid,point,type,department,cname,prof,grade from class where year = ? and term = ?', [year, term], function(err, classrow) {
        if (err) {
          connection.release()
          log(err)
          //util.newerror(code + '008')
          if (typeof callback === 'function') callback(0)
        } else {
          rows.class = classrow
          connection.query('select bname,rid,cid,cday,speriod,eperiod from classtable where year = ? and term = ?', [year, term], function(err, classtablerow) {
            if (err) {
              connection.release()
              log(err)
              //util.newerror(code + '009')
              if (typeof callback === 'function') callback(0)
            } else {
              rows.classtable = classtablerow
              rows.classroom = classroomrow
              connection.query('select period,stime,etime from schedule where year = ? and term = ?' [year, term], function(err, schedulerow) {
                connection.release()
                if (err) {
                  log(err)
                  //util.newerror(code + '012')
                  if (typeof callback === 'function') callback(0)
                } else {
                  rows.schedule = schedulerow
                  if (typeof callback === 'function') callback(rows)
                }
              })
            }
          })
        }
      })
    }
  })
}

function getunivlist(callback) {
  util.getmainp().getConnection(function(err, connection) {
    if (err) {
      log(err)
      //util.newerror('000015')
      if (typeof callback === 'function') callback(0)
    } else {
      connection.query('select * from univlist', function(err, rows) {
        connection.release()
        if (err) {
          log(err)
          //util.newerror('000016')
          if (typeof callback === 'function') callback(0)
        } else {
          if (typeof callback === 'function') callback(rows)
        }
      })
    }
  })
}

function getdepartmentlist(ucode, callback) {
  util.setpool(ucode)
  util.getpool(ucode).getConnection(function(err, connection) {
    if (err) {
      log(err)
      //util.newerror(code + '017')
      if (typeof callback === 'function') callback(0)
    } else {
      connection.query('select * from department', function(err, rows) {
        connection.release()
        if (err) {
          log(err)
          //util.newerror(code + '018')
          if (typeof callback === 'function') callback(0)
        } else {
          if (typeof callback === 'function') callback(rows)
        }
      })
    }
  })
}

function log(msg) {
  util.log(msg)
}

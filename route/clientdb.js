var util = require('./util')

exports.newclassp = newclassp
exports.signin = signin
exports.kakaosignin = kakaosignin
exports.newclient = newclient
exports.getuserinfo = getuserinfo
exports.isauth = isauth
exports.feedback = feedback
exports.getfeedback = getfeedback


function newclassp(android,ios,ucode,callback) {
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('insert into classpclient set ?',{'android' : android,'ios':ios,'ucode':ucode},
			function(err,result) {
				connection.release()
				if(err) {
					log(err)
					if(typeof callback === 'function') callback(0)
				} else {
					if(typeof callback === 'function') callback(1)
				}
			})
		}
	})
}

function signin(email,kind,callback) {
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			//util.newerror('000000')
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('SELECT scode,ucode from clients where email = ? and kind = ?',[email,kind],
			function(err,rows) {
				connection.release()
				if(err) {
					log(err)
					//util.newerror('000001')
					if(typeof callback === 'function') callback(0)
				} else {
					if(rows.length == 0){
						if(typeof callback === 'function') callback(3)
					} else {
						if(typeof callback === 'function') callback(rows[0])
					}
				}
			})
		}
	})
}

function kakaosignin(kakaoid,callback) {
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			//util.newerror('000000')
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('SELECT email,scode,ucode from clients where kakaoid = ?',[kakaoid],
			function(err,rows) {
				connection.release()
				if(err) {
					log(err)
					//util.newerror('000001')
					if(typeof callback === 'function') callback(0)
				} else {
					if(rows.length == 0){
						if(typeof callback === 'function') callback(3) //new
					} else {
						if(typeof callback === 'function') callback(rows[0])
					}
				}
			})
		}
	})
}

// function getsession(email,kind,callback) {
// 	util.getmainp().getConnection(function(err,connection) {
// 		if(err) {
// 			log(err)
// 			//util.newerror('000000')
// 			if(typeof callback === 'function') callback(0)
// 		} else {
// 			connection.query('select scode,ucode from clients where email = ? and kind = ?',[email,kind],
// 			function(err,rows) {
// 				connection.release()
// 				if(err) {
// 					log(err)
// 					//util.newerror('000001')
// 					if(typeof callback === 'function') callback(0)
// 				} else {
// 					if(rows.length == 0){
// 						if(typeof callback === 'function') callback('no')
// 					} else {
// 						if(typeof callback === 'function') callback(rows[0])
// 					}
// 				}
// 			})
// 		}
// 	})
// }

function newclient(email,kind,kakaoid,ucode,department,ddepartment,snum,name,sex,callback) {
	util.setpool(ucode)
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			//util.newerror('000002')
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('insert into clients set ?',{'email' : email, 'kind' : kind,'kakaoid' : kakaoid, 'ucode' : ucode,
			'sex' : sex, 'name' : name, 'sdate' : util.nowdate(), 'auth' : 0},
			function(err,result) {
				if(err) {
					connection.release()
					log(err)
					//util.newerror('000003')
					if(typeof callback === 'function') callback(0)
				} else {
					connection.query('select scode from clients where email = ? and kind = ?',[email,kind],
					function(err,rows) {
						connection.release()
						if(err) {
							log(err)
							if(typeof callback ==='function') callback(err)
						} else {
							var scode = rows[0].scode
							util.getpool(ucode).getConnection(function(err,connection) {
								if(err) {
									log(err)
									//util.newerror(ucode+'004')
									if(typeof callback === 'function') callback(0)
								} else {
									connection.query('insert into student set ?', {'scode' : scode, 'name' : name, 'department' : department, 'ddepartment' : ddepartment, 'snum' : snum},
									function(err,result) {
										connection.release()
										if(err) {
											log(err)
											//util.newerror(ucode+'005')
											if(typeof callback === 'function') callback(0)
										} else {
											if(typeof callback === 'function') callback(scode)
										}
									})
								}
							})
						}
					})
				}
			})
		}
	})
}

function getuserinfo(scode,ucode,callback) {
	util.setpool(ucode)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('select * from student where scode = ?',[scode],
			function(err,rows) {
				connection.release()
				if(err) {
					log(err)
					//util.newerror(code+'999')
					if(typeof callback === 'function') callback(0)
				} else {
					callback(rows[0])
				}
			})
		}
	})
}

function feedback(category,text,os,ver,callback) {
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('insert into feedback set ?',{'category':category,'date':util.nowtime(),'contents':text,'os':os,'ver':ver},
			function(err,result) {
				connection.release()
				if(err) {
					log(err)
					if(typeof callback === 'function') callback(0)
				} else {
					if(typeof callback === 'function') callback(1)
				}
			})
		}
	})
}

function getfeedback(callback) {
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback === 'function') callback(0)
		} else {
			connection.query('select * from feedback',
			function(err,rows){
				connection.release()
				if(err) {
					log(err)
					if(typeof callback === 'function') callback(0)
				} else {
					if(typeof callback === 'function') callback(rows)
				}
			})
		}
	})
}

function isauth(scode,ucode,callback) {
	util.getmainp().getConnection(function(err,connection) {
		if(err) {
			log(err)
			callback(0)
		} else {
			connection.query('select auth from clients where scode = ? and ucode = ?',[scode,ucode],function(err,rows) {
				if(err) {
					connection.release()
					log(err)
					callback(0)
				} else {
					if(rows[0].auth == 1) {
						connection.release()
						callback(2) //already
					} else {
						connection.query('select email from univlist where ucode = ?',[ucode],function(err,rows) {
							connection.release()
							if(err) {
								log(err)
								callback(0)
							} else {
								callback(rows[0].email)
							}
						})
					}
				}
			})
		}
	})
}

function log(msg) {
	util.log(msg)
}

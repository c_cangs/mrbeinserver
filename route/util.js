var mysql = require('mysql');

exports.nowtime = nowtime;
exports.newerror = newerror;
exports.nowdate = nowdate;
exports.setLogStream = setLogStream;
exports.slog = slog
exports.mlog = mlog
exports.log = log;
exports.setpool = setpool;
exports.getpool = getpool;
exports.getmainp = getmainp;


var pools = new Array();
var vers = new Array();

var accessLogStream;

var mainpool = mysql.createPool({
	host : 'localhost',
	port : 3306,
	user : 'root',
	password : 'classsql',
	database : 'classpick'
});

var errorpool = mysql.createPool({
	host : 'localhost',
	port : 3306,
	user : 'root',
	password : 'classsql',
	database : 'errors'
});

function setpool(ucode){
	if(pools[ucode] == undefined || pools[ucode] == null){
	    pools[ucode] = mysql.createPool({
			host : 'localhost',
			port : 3306,
			user : 'root',
			password : 'classsql',
			database : ucode
		});
		pools[ucode].getConnection(function(err,connection) {
			if(err){
				log(err);
			} else {
				connection.query('select max(ver) from uversion', function(err,rows) {
					if(err) {
						log(err);
					} else {
						vers[ucode] = rows[0].ver;
					}
				});
			}
		});
	}
}

function getpool(ucode) {
	return pools[ucode];
}

function getmainp() {
	return mainpool;
}

function setLogStream(Stream) {
	accessLogStream = Stream;
}

function nowtime(){
  now = new Date();
  year = now.getFullYear();
  month= now.getMonth() + 1;
  if(month < 10){
    month = '0' + month;
  }
  date = now.getDate();
  if(date<10){
    date = '0' + date;
  }
  hour = now.getHours();
  if(hour<10){
    hour = '0' + hour;
  }
  min = now.getMinutes();
  if(min<10){
    min = '0' + min;
  }
  sec = now.getSeconds();
  if(sec<10){
    sec = '0' + sec;
  }

  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function nowdate() {
	now = new Date();
	year = now.getFullYear();
	month= now.getMonth() + 1;
	if(month < 10){
		month = '0' + month;
	}
	date = now.getDate();
	if(date<10){
		date = '0' + date;
	}
	return year+'-'+month+'-'+date;
}

function newerror(errcode) {
	errorpool.getConnection(function(err,connection) {
		if(err) console.log(nowtime() + 'errorpool error1');
		else {
			connection.query('insert into errors set ?',{"code":errcode,"datetime":nowtime()},
			function(err,result) {
				connection.release();
				if(err) console.log(nowtime() + 'errorpool error2');
			});
		}
	});
}

function slog(wid,msg) {
	logm = nowtime() + ' worker ' + wid + ' : ' + msg;
	if(!(accessLogStream == null || accessLogStream == undefined)) accessLogStream.write(logm+'\n');
	console.log(logm);
}

function mlog(wid,req,res) {
	time = Date.now() - req.reqtime
	logm = nowtime() + ' worker ' + wid + ' ' + req.connection.remoteAddress + ' ' + req.method + req.originalUrl + ' ' + res.statusCode + ' ' + time
	if(!(accessLogStream == null || accessLogStream == undefined)) accessLogStream.write(logm+'\n')
	console.log(logm)
}

function log(msg) {
	if(!(accessLogStream == null || accessLogStream == undefined)) accessLogStream.write(nowtime() + ' - ' + msg+'\n');
	console.log(msg);
}

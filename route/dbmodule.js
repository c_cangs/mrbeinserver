var mysql = require('mysql');
var util = require('./util');

var pools = new Array();

exports.setpool = setpool;
exports.signin = signin;
exports.newclient = newclient;
exports.getuserinfo = getuserinfo;
exports.getdb = getdb;
exports.getdbV = getdbV;
exports.getunivlist = getunivlist;
exports.getdepartmentlist = getdepartmentlist;
exports.getfavorite = getfavorite;
exports.insertfavorite = insertfavorite;
exports.deletefavorite = deletefavorite;


var mainpool = mysql.createPool({
	host : 'localhost',
	port : 3306,
	user : 'root',
	password : 'classsql',
	database : 'main'
});

function setpool(code){
	var ncode = Number(code);
	if(pools[ncode] == undefined){
	    pools[ncode] = mysql.createPool({
			host : 'localhost',
			port : 3306,
			user : 'root',
			password : 'classsql',
			database : code
		});
	}
}

function signin(email,kind,callback) {
	mainpool.getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror('000000');
			if(typeof callback === 'function') callback('err');
		} else {
			connection.query('select email,kind,scode from clients where email = ? and kind = ?',[email,kind],
			function(err,rows) {
				connection.release();
				if(err) {
					log(err);
					util.newerror('000001');
					if(typeof callback === 'function') callback('err');
				} else {
					if(rows.length == 0){
						if(typeof callback === 'function') callback('new');
					} else {
						if(typeof callback === 'function') callback(rows[0].scode);
					}
				}
			});
		}
	});
}

function newclient(email,kind,code,department,ddepartment,callback) {
	var ncode = Number(code);
	setpool(code);
	mainpool.getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror('000002');
			if(typeof callback === 'function') callback('err');
		} else {
			connection.query('insert into clients set ?',{'email' : email, 'kind' : kind, 'scode' : code},
			function(err,result) {
				connection.release();
				if(err) {
					log(err);
					util.newerror('000003');
					if(typeof callback === 'function') callback('err');
				} else {
					pools[ncode].getConnection(function(err,connection) {
						if(err) {
							log(err);
							util.newerror(code+'004');
							if(typeof callback === 'function') callback('err');
						} else {
							connection.query('insert into student set ?', {'email' : email, 'kind' : kind, 'department' : department, 'ddepartment' : ddepartment},
							function(err,result) {
								connection.release();
								if(err) {
									log(err);
									util.newerror(code+'005');
									if(typeof callback === 'function') callback('err');
								} else {
									if(typeof callback === 'function') callback('suc');
								}
							});
						}
					});
				}
			});
		}
	});
}

function getuserinfo(email,kind,code,callback) {
	var ncode = Number(code);
	setpool(code);
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			if(typeof callback === 'function') callback('err');
		} else {
			connection.query('select * from student where email = ? and kind = ?',[email,kind],
			function(err,rows) {
				if(err) {
					connection.release();
					log(err);
					util.newerror(code+'999');
					if(typeof callback === 'function') callback('err');
				} else {
					if(typeof callback === 'function') callback(rows[0]);
				}
			});
		}
	});
}


function getdb(code,callback) {
	var ncode = Number(code);
	setpool(code);
	var year;
	var term;
	var rows = new Array();
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror(code+'006');
			if(typeof callback ==='function') callback('err');
		} else {
			connection.query('select * from etc where version = (select max(version) from etc)',function(err,etcrow) {
				if(err) {
					connection.release();
					log(err);
					util.newerror(code+'007');
					if(typeof callback ==='function') callback('err');
				} else {
					rows[0] = etcrow;
					year = etcrow[0].year;
					term = etcrow[0].term;
					connection.query('select cid,point,type,department,cname,prof,grade from class where year = ? and term = ?',[year,term],
					function(err,classrow) {
						if(err) {
							connection.release();
							log(err);
							util.newerror(code + '008');
							if(typeof callback ==='function') callback('err');
						} else {
							rows[1] = classrow;
							connection.query('select bname,rid,cid,cday,speriod,eperiod from classtable where year = ? and term = ?',[year,term],
							function(err,classtablerow) {
								if(err) {
									connection.release();
									log(err);
									util.newerror(code + '009');
									if(typeof callback ==='function') callback('err');
								} else {
									rows[2] = classtablerow;
									connection.query('select * from building',function(err,buildingrow) {
										if(err) {
											connection.release();
											log(err);
											util.newerror(code + '010');
											if(typeof callback ==='function') callback('err');
										} else {
											rows[3] = buildingrow;
											connection.query('select * from classroom',function(err,classroomrow) {
												if(err) {
													connection.release();
													log(err);
													util.newerror(code + '011');
													if(typeof callback ==='function') callback('err');
												} else {
													rows[4] = classroomrow;
													connection.query('select * from schedule',function(err,timetablerow) {
														connection.release();
														if(err) {
															log(err);
															util.newerror(code + '012');
															if(typeof callback ==='function') callback('err');
														} else {
															rows[5] = timetablerow;
															if(typeof callback ==='function') callback(rows);
														}
													});
												}
											});
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
}

function getdbV (code,callback) {
	var ncode = Number(code);
	setpool(code);
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror(code + '013');
			if(typeof callback ==='function') callback('err');
		} else {
			connection.query('select max(version) as version from etc', function (err,rows) {
				connection.release();
				if(err) {
					log(err);
					util.newerror(code + '014');
					if(typeof callback ==='function') callback('err');
				} else {
						if(typeof callback ==='function') callback(rows[0].version);
				}
			});
		}
	});
}
	
function getunivlist(callback) {
	mainpool.getConnection(function(err,connection) {
		if(err){
			log(err);
			util.newerror('000015');
			if(typeof callback ==='function') callback('err');
		} else {
			connection.query('select * from univlist',function(err,rows) {
				connection.release();
				if(err) {
					log(err);
					util.newerror('000016');
					if(typeof callback ==='function') callback('err');
				} else {
					if(typeof callback ==='function') callback(rows);
				}
			});
		}
	});
}
		
function getdepartmentlist(code,callback) {
	var ncode = Number(code);
	setpool(code);
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror(code + '017');
			if(typeof callback ==='function') callback('err');
		} else {
			connection.query('select * from department',function(err,rows) {
				connection.release();
				if(err) {
					log(err);
					util.newerror(code + '018');
					if(typeof callback ==='function') callback('err');
				} else {
					if(typeof callback ==='function') callback(rows);
				}
			});
		}
	});
}

function getfavorite(email,kind,code,callback) {
	var ncode = Number(code);
	setpool(code);
	var rows = new Array();
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror(code + '019');
			if(typeof callback ==='function') callback('err');
		} else {
			connection.query('select bname from bfavorite where email = ? and kind = ?',[email,kind],
			function(err,bfrows) {
				if(err) {
					connection.release();
					log(err);
					util.newerror(code + '020');
					if(typeof callback ==='function') callback('err');
				} else {
					rows[0] = bfrows;
					connection.query('select bname,rid from rfavorite where email = ? and kind = ?',[email,kind],
					function(err,rfrows) {
						connection.release();
						if(err) {
							log(err);
							util.newerror(code + '021');
							if(typeof callback ==='function') callback('err');
						} else {
							rows[1] = rfrows;
							if(typeof callback ==='function') callback(rows);
						}
					});
				}
			});
		}
	});
}

function insertfavorite(email,kind,bname,rid,code,callback) {
	var ncode = Number(code);
	setpool(code);
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror(code + '022');
			if(typeof callback ==='function') callback('err');
		} else {
			if(rid == undefined||rid == null) {
				connection.query('insert into bfavorite set ?',{"email" : email,"kind" : kind,"bname" : bname},
				function(err,results) {
					connection.release();
					if(err){
						log(err);
						util.newerror(code + '023');
						if(typeof callback ==='function') callback('err');
					} else {
						if(typeof callback ==='function') callback('building');
					}
				});
			} else if(rid != undefined|| rid != null) {
				connection.query('insert into rfavorite set ?',{"email" : email,"kind" : kind,"bname" : bname,"rid" : rid},
				function(err,results) {
					connection.release();
					if(err) {
						log(err);
						util.newerror(code + '024');
						if(typeof callback ==='function') callback('err');
					} else {
						if(typeof callback ==='function') callback('room');
					}
				});
			} else {
				util.newerror(code + '025');
				if(typeof callback ==='function') callback('err');
			}
		}
	});
}

function deletefavorite(email,kind,bname,rid,code,callback) {
	var ncode = Number(code);
	setpool(code);
	pools[ncode].getConnection(function(err,connection) {
		if(err) {
			log(err);
			util.newerror(code + '026');
			if(typeof callback ==='function') callback('err');
		} else {
			if(rid == undefined || rid == null) {
				connection.query('delete from bfavorite where email = ? and kind = ? and bname = ?',[email,kind,bname],
				function(err,results) {
					connection.release();
					if(err){
						log(err);
						util.newerror(code + '027');
						if(typeof callback ==='function') callback('err');
					} else {
						if(typeof callback ==='function') callback('building');
					}
				});
			} else if(rid != undefined || rid != null) {
				connection.query('delete from rfavorite where email = ? and kind = ? and bname = ? and rid = ?',[email,kind,bname,rid],
				function(err,results) {
					connection.release();
					if(err) {
						log(err);
						util.newerror(code + '028');
						if(typeof callback ==='function') callback('err');
					} else {
						if(typeof callback ==='function') callback('room');
					}
				});
			} else {
				util.newerror(code + '029');
				if(typeof callback ==='function') callback('err');
			}
		}
	});
}
						
									

function log(msg) {
	console.log(util.nowtime() + 'kr : ' + msg);
}

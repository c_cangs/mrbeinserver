var util = require('./util')

exports.addclass = addclass
exports.deleteclass = deleteclass
exports.updatepoint = updatepoint
exports.getstimetable = getstimetable

function addclass (ucode,scode,year,term,cid,spoint,callback) {
	util.setpool(ucode)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback ==='function') callback(0)
		} else {
			if(spoint == null || spoint == undefined) {
				connection.query("insert into stimetable set ?",{"scode" : scode,"cid" : cid,"year" : year,"term" : term,"spoint" :  -1},
				function(err,results) {
					connection.release()
					if(err) {
						log(err)
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(1)
					}
				})
			} else {
				connection.query("insert into stimetable set ?",{"scode" : scode,"cid" : cid,"year" : year,"term" : term,"spoint" :  spoint},
				function(err,results) {
					connection.release()
					if(err) {
						log(err)
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(1)
					}
				})
			}
		}
	})
}

function deleteclass (ucode,scode,year,term,cid,callback) {
	util.setpool(ucode)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback ==='function') callback(0)
		} else {
			connection.query("delete from stimetable where scode = ? and cid = ? and year = ? and term = ?",[scode,cid,year,term],
			function(err,results) {
				connection.release()
				if(err) {
					log(err)
					if(typeof callback ==='function') callback(0)
				} else {
					if(typeof callback ==='function') callback(1)
				}
			})
		}
	})
}

function updatepoint(ucode,scode,year,term,list,callback) {
	util.setpool(ucode)
	var json = JSON.parse(list)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback ==='function') callback(0)
		} else {
			for(var x in json) {
				connection.query("update stimetable set spoint = ? where scode = ? and year = ? and term = ? and cid = ?",[json[x]["spoint"],scode,year,term,json[x]["cid"]],
				function(err,results) {
					if(err) {
						connection.release()
						log(err)
						if(typeof callback ==='function') callback(0)
						break
					}
					if(x == json.length - 1) {
						connection.release()
						if(typeof callback ==='function') callback(1)
					}
				})
			}
		}
	})
}

function getstimetable(ucode,scode,year,term,cid,callback) {
	util.setpool(ucode)
	util.getpool(ucode).getConnection(function(err,connection) {
		if(err) {
			log(err)
			if(typeof callback ==='function') callback(0)
		} else {
			if(year == null || year == undefined || term == null || term == undefined) {
				connection.query("select year,term,cid,spoint from stimetable where scode = ? ",[scode],
				function(err,rows) {
					connection.release()
					if(err) {
						log(err)
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(rows)
					}
				})
			} else if(cid != null && cid != undefined) {
				connection.query("select year,term,cid,spoint from stimetable where scode = ? and cid = ? and year = ? and term = ? ",[scode,cid,year,term],
				function(err,rows) {
					connection.release()
					if(err) {
						log(err)
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(rows)
					}
				})
			} else {
				connection.query("select year,term,cid,spoint from stimetable where scode = ? and year = ? and term = ? ",[scode,year,term],
				function(err,rows) {
					connection.release()
					if(err) {
						log(err)
						if(typeof callback ==='function') callback(0)
					} else {
						if(typeof callback ==='function') callback(rows)
					}
				})
			}
		}
	})
}

function log(msg) {
	util.log(msg)
}

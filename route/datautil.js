const fs = require('fs')
const schedule = require('node-schedule')
const util = require('./util')

const path = '/root/classp/classpick-server'

var logger
var appVmsg
var notice

exports.getAppVmsg = getAppVmsg
exports.getNotice = getNotice

function set_log_config() {
  logfn = path + '/mrbeinlog/' + util.nowdate() + '.log'
  if (!(logger == undefined || logger == null)) logger.end()
  if(fs.existsSync(logfn)) logger = fs.createWriteStream(logfn,{flags : 'a'})
  else logger = fs.createWriteStream(logfn,{flags : 'w'})
  util.setLogStream(logger)
}

function dataRead() {
  fs.readFile(path + '/data/appVmsg2.json','utf8',function(err,data) {
    if(err) {
      util.log('appVmsg read error')
      util.newerror('000001')
      process.exit();
    } else {
      appVmsg = data;
    }
  })
  fs.readFile(path + '/data/notice.json','utf8',function(err,data) {
    if(err) {
      util.log('notice read error')
      util.newerror('000001')
      process.exit();
    } else {
      notice = data;
    }
  })
}

dataRead()
set_log_config()

setInterval(function() {dataRead()},30000)

schedule.scheduleJob({
  hour:0,
},function() {
  if(!(logger == null || logger == undefined)) set_log_config()
})

setInterval(function(){
  dataRead()
},30000)

function getAppVmsg() {
  return appVmsg
}

function getNotice() {
  return notice
}

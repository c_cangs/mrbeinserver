/*packages*/
//rest api
const express = require('express')
const request = require('request')
const cluster = require('express-cluster')
const session = require('express-session')
const bodyparser = require('body-parser')
//firebase
const admin = require('firebase-admin')
const serviceAccount = require('./data/key.json')
//redis for session
const redis = require('redis')
const redissession = require('connect-redis')(session)
const redisclient = redis.createClient()
//auth for login
const keys = require('./data/privatekey.json')
const GoogleAuth = require('google-auth-library')
const FacebookTokenStrategy = require('passport-facebook-token')
const passport = require('passport')
//route modules
const util = require('./route/util')
const datautil = require('./route/datautil')
const univR = require('./route/univdb')
const clientR = require('./route/clientdb')
const favoriteR = require('./route/favoritedb')
const univtableR = require('./route/univtabledb')
const mailauth = require('./route/mailauth')

//init firebase
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://class-pick-40044.firebaseio.com/"
})
//session check
const issession = function(req,res,next) {
  if(req.session.auth == true) return next()
  else return res.sendStatus(401)
}
//login auth setting
//google
const auth = new GoogleAuth
const clientidandroid = keys.googleandroid
const clientidios = keys.googleios
const client = new auth.OAuth2(clientidandroid,clientidios, '')
//facebook
passport.use(new FacebookTokenStrategy({
        clientID: keys.facebookid,
        clientSecret: keys.facebooksecret,
    },
    function(accessToken, refreshToken, profile, done) {
        return done(null,profile.emails[0].value)
    }
))
mailauth.getredis(redisclient)

/*server*/
cluster(function (worker) {
  //cluster start
	const app = express()
	var id = worker.id
	log(id,'worker start')
  /*middleware setting*/
  //set bodyparser
	app.set(function(){app.use(express.bodyParser())})
	app.use(bodyparser.urlencoded({extended:false}))
  //set session
  app.use(session(
		{
			secret : 'hyjed@12554@',
			store : new redissession({
				host: 'localhost',
				port: 6379,
				client : redisclient,
				prefix : 'session:',
				db : 0,
				ttl:1200
			}),
			saveUninitialized: false,
			resave: false
		}
	))
  //middleware for logging
	app.use(function(req,res,next) {
	  req.reqtime = Date.now()
	  res.on('finish',function() {
			util.mlog(id,req,res)
	  })
	  next()
	})
  //server start
	app.listen(8008)

  /*server func*/
  //check session
	app.get('/issession',issession,function(req,res,next) {
		res.send(String(1))
	})
  //send app info and msg
  app.get('/appVmsg',function(req,res,next) {
		res.set('Content-Type', 'application/json charset=utf-8')
    res.send(datautil.getAppVmsg())
	})
  //gmail login
  app.post('/gmaillogin',function(req,res,next){
		var token = req.body.token
		client.verifyIdToken(token,[clientidandroid,clientidios],function(e,login){
			if(e){
				log(id,e)
				util.newerror('900100')
				res.status(500).send(String(0))
			} else {
				var payload = login.getPayload()
				clientR.signin(payload['email'],'gmail',function(msg) {
					if(msg == 3) {
						req.session.auth = true
						req.session.user = payload['email']
						req.session.kind = 'gmail'
						res.send(String(msg))
					} else if(msg == 0) {
						res.status(500).send(String(msg))
					} else {
						req.session.auth = true
						req.session.ucode = msg.ucode
						req.session.scode = msg.scode
            clientR.getuserinfo(msg.scode,msg.ucode,function(info) {
              if(info != 0) {
                clientR.isauth(msg.scode,msg.ucode,function(au) {
                  if(au !=0) {
                    res.set('Content-Type', 'application/json charset=utf-8')
                    info.isauth = au
                    res.send(info)
                  } else res.status(500).send(String(0))
                })
              }
              else res.status(500).send(String(0))
            })
					}
				})
			}
		})
	})
  //facebook login
  app.post('/facebooklogin',passport.authenticate('facebook-token',{session: false}),function(req,res) {
		if(req.user) {
			clientR.signin(req.user,'facebook',function(msg) {
				if(msg == 3) {
					req.session.auth = true
					req.session.user = req.user
					req.session.kind = 'facebook'
					res.send(String(msg))
				} else if(msg == 0) {
					res.status(500).send(String(msg))
				} else {
					req.session.auth = true
					req.session.scode = msg.scode
					req.session.ucode = msg.ucode
          clientR.getuserinfo(msg.scode,msg.ucode,function(info) {
            if(info != 0) {
              clientR.auth(msg.scode,msg.ucode,function(au) {
                if(au !=0) {
                  res.set('Content-Type', 'application/json charset=utf-8')
                  info.isauth = au
                  res.send(info)
                } else res.status(500).send(String(0))
              })
            }
            else res.status(500).send(String(0))
          })
				}
			})
		} else {
			util.newerror('900101')
			res.status(500).send(String(0))
		}
	})
	//kakao login
	app.post('/kakaologin',function(req,res,next) {
		var token = req.body.token
		request({
		  url: 'https://kapi.kakao.com/v1/user/access_token_info',
		  headers: {
		    'Authorization': 'Bearer ' + token
		  }
		},function(err,response,body) {
			if(!err && response.statusCode == 200) {
				var json = JSON.parse(body)
				clientR.kakaosignin(json.id,function(msg) {
					if(msg == 3) {
						req.session.auth = true
						req.session.user = msg.email
						req.session.kind = 'kakao'
						req.session.kakaoid = json.id
						res.send(String(msg))
					} else if(msg == 0) {
						res.status(500).send(String(0))
					} else {
						req.session.auth = true
						req.session.scode = msg.scode
						req.session.ucode = msg.ucode
            clientR.getuserinfo(msg.scode,msg.ucode,function(info) {
              if(info != 0) {
                clientR.isauth(msg.scode,msg.ucode,function(au) {
                  if(au !=0) {
                    res.set('Content-Type', 'application/json charset=utf-8')
                    info.isauth = au
                    res.send(info)
                  } else res.status(500).send(String(0))
                })
              }
              else res.status(500).send(String(0))
            })
					}
				})
			} else {
				res.status(500).send(String(0))
			}
		})
	})
  //logout
  app.get('/logout',issession,function(req,res,next) {
    req.session.destory(function(err) {
      if(err) res.status(500).send(String(0))
      else res.send(String(1))
    })
  })
  //new user no login
	app.get('/newnologinuser',function(req,res,next) {
		var android = req.query.android
		var ios = req.query.ios
		var ucode = req.query.ucode
		res.set('Content-Type','text/plain')
		clientR.newclassp(android,ios,ucode,function(msg) {
			if(msg == 0) {
				res.status(500).send(String(msg))
			} else {
				res.send(String(msg))
			}
		})
	})
  //new user
  app.post('/newclient',issession,function(req,res,next) {
    res.set('Content-Type','text/plain')
		var ucode = req.body.ucode
		var department = req.body.department
		var ddpartment = req.body.ddepartment
		var snum = req.body.snum
		var name = req.body.name
		var sex = req.body.sex
		var kakaoid
		if(req.session.kakaoid == null || req.session.kakaoid == undefined) {
			kakaoid = -1
		} else kakaoid = req.session.kakaoid
		clientR.newclient(req.session.user,req.session.kind,kakaoid,ucode,department,ddpartment,snum,name,sex,
			function(msg) {
			if(msg != 0) {
				req.session.scode = msg
        clientR.getuserinfo(msg.scode,msg.ucode,function(info) {
          if(info != 0) {
            clientR.isauth(msg.scode,msg.ucode,function(au) {
              if(au !=0) {
                res.set('Content-Type', 'application/json charset=utf-8')
                info.isauth = au
                res.send(info)
              } else res.status(500).send(String(0))
            })
          }
          else res.status(500).send(String(0))
        })
			}
			else res.status(500).send(String(0))
		})
	})
	//send notice
	app.get('/notice',issession,function(req,res,next) {
		res.set('Content-Type', 'application/json charset=utf-8')
    res.send(datautil.getNotice())
	})
  //send user info
  app.get('/getuserinfo',issession,function(req,res,next) {
		clientR.getuserinfo(req.session.scode,req.session.ucode,function(msg) {
			if(msg == 0) {
				res.set('Content-Type','text/plain')
				res.status(500).send(String(0))
			}
			else {
				res.set('Content-Type','application/json charset=utf-8')
				res.send(msg)
			}
		})
	})
  //send univ db
	app.get('/getdb',issession,function(req,res,next) {
		univR.getdb(req.session.ucode,function(msg) {
			if(msg === 0 ) {
				res.set('Content-Type', 'text/plain')
				res.status(500).send(String(msg))
			}
			else {
				res.set('Content-Type', 'application/json charset=utf-8')
				res.send(msg)
			}
		})
	})
  //send univ db version
	app.get('/getdbV',issession,function(req,res,next) { //�б� db����
		res.set('Content-Type', 'text/plain')
		univR.getdbV(req.session.ucode,function(msg) {
			if(msg == 0) {
				res.status(500).send(String(msg))
			} else {
				res.send(String(msg))
			}
		})
	})
  //send univ db year term
  app.get('/getdbC',issession,function(req,res,next) {
    univR.getdb(req.session.ucode,function(msg) {
			if(msg === 0 ) {
				res.set('Content-Type', 'text/plain')
				res.status(500).send(String(msg))
			}
			else {
				res.set('Content-Type', 'application/json charset=utf-8')
				res.send(msg)
			}
		})
  })
  //send univlist
	app.get('/getunivlist',issession,function(req,res,next) {//�б� ����Ʈ
		univR.getunivlist(function(msg) {
			if(msg === 0) {
				res.set('Content-Type', 'text/plain')
				res.status(500).send(String(msg))
			} else {
				res.set('Content-Type', 'application/json charset=utf-8')
				res.send(msg)
			}
		})
	})
  //check auth
  app.get('/isauth',issession,function(req,res,next) {
    res.set('Content-Type', 'text/plain')
    clientR.isauth(req.session.scode,req.session.ucode,function(msg) {
      if(msg == 0) res.status(500).send(String(msg))
      else res.send(String(msg))
    })
  })
  //start auth send mail
  app.post('/startauth',issession,function(req,res,next) {
    res.set('Content-Type', 'text/plain')
    mailauth.startauth(req.session.scode,req.body.email,function(msg) {
      if(msg == 0) res.status(500).send(String(msg))
      else res.send(String(msg))
    })
  })
  app.get('/testauth',function(req,res,next) {
    mailauth.test(1,req.query.email,function(msg) {
      if(msg == 0) res.status(500).send(String(msg))
      else res.send(String(msg))
    })
  })
  app.get('/authtest',function(req,res,next) {
    mailauth.verifytest(req.query.code,req.query.id,function(msg) {
      if(msg == 0) res.send('인증실패')
      else res.send('인증성공')
    })
  })
  //do auth client
  app.get('/authmail',function(req,res,next) {
    mailauth.verifyauth(req.query.code,req.query.id,function(msg) {
      if(msg == 0) res.send('인증실패 인증과정을 처음부터 다시 시도해 주세요')
      else if(msg == 1) res.send('인증성공')
      else if(msg == 2) res.send('이미 인증이 되어있습니다')
      else res.sendStatus(500)
    })
  })
  //get feedback
	app.get('/feedback',issession,function(req,res,nect) {
		res.set('Content-Type', 'text/plain')
		clientR.feedback(req.query.category,req.query.contents,req.query.os,req.query.ver,function(msg) {
			if(msg == 0) {
				res.status(500).send(String(msg))
			} else {
				res.send(msg)
			}
		})
	})
  //send feedbacklist
  app.get('/feedbacklist',function(req,res,next) {
    clientR.getfeedback(function(msg) {
      if(msg == 0) {
        res.set('Content-Type', 'text/plain')
				res.status(500).send(String(msg))
			} else {
        res.set('Content-Type', 'application/json charset=utf-8')
				res.send(msg)
			}
    })
  })
  //block all ather
	app.all('*',function(req,res,next){
		res.sendStatus(404)
	})
} , {count:3})


function log(wid,msg){
	util.slog(wid,msg)
}

var express = require('express')

var app = express()

app.listen(8008)

app.use(function(req,res,next) {
  req.reqtime = Date.now()
  res.on('finish',function() {
    console.log(req.connection.remoteAddress)
    console.log(req.originalUrl)
    console.log(res.statusCode)
    console.log(Date.now() - req.reqtime)
    console.log(req)
  })
  next()
})

app.get('/a',function(req,res,next) {
  res.send(String(1))
})

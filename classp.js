var express = require('express');
var cluster = require('express-cluster');
var bodyparser = require('body-parser');
var morgan = require('morgan');
var admin = require('firebase-admin');
var serviceAccount = require('./data/key.json')

var util = require('./route/util');
var univR = require('./route/univdb');
var clientR = require('./route/clientdb');
var favoriteR = require('./route/favoritedb');
var univtableR = require('./route/univtabledb');

var fs = require('fs');
var path = require('path');
var rfs = require('rotating-file-stream');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://class-pick-40044.firebaseio.com/"
});

var logDirectory = path.join(__dirname, 'log');

fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

var accessLogStream = rfs(util.nowdate()+'.log', {
	interval: '1d',
	size: '10M',
	path : logDirectory
});

util.setLogStream(accessLogStream);

morgan.token('ktime',function(){
	return util.nowtime();
});

cluster(function (worker) {
	var app = express();
	var id = worker.id;
	log(id,'worker start');
	app.set(function(){app.use(express.bodyParser());});
	app.use(bodyparser.urlencoded({extended:false}));

	app.use(morgan(':ktime worker '+id+' :remote-addr :method:url :status :response-time ms'));
	app.use(morgan(':ktime worker '+id+' :remote-addr :method:url :status :response-time ms',{stream: accessLogStream}));
	app.use(express.static(__dirname+'/data'));
	app.listen(8010);

	app.get('/newclasspclient',function(req,res,next) {//ȸ������ - �б��ڵ�, �̸�, �г���, �а�, ��������
		var android = req.query.android;
		var ios = req.query.ios;
		var ucode = req.query.ucode;
		res.set('Content-Type','text/plain');
		clientR.newclassp(android,ios,ucode,function(msg) {
			if(msg == 'err') {
				res.status(500).send(msg);
			} else {
				res.send(msg);
			}
			res.end();
		});
	});

	app.get('/getdb',function(req,res,next) {//�б� db
		univR.getdb(req.query.ucode,function(msg) {
			if(typeof(msg) === 'string' ) {
				res.set('Content-Type', 'text/plain');
				res.status(500).send(msg);
			}
			else {
				res.set('Content-Type', 'application/json; charset=utf-8');
				res.send(msg);
			}
			res.end();
		});
	});

	app.get('/getdbV',function(req,res,next) { //�б� db����
		res.set('Content-Type', 'text/plain');
		univR.getdbV(req.query.ucode,function(msg) {
			if(msg == 'err') {
				res.status(500).send(msg);
			} else {
				res.send(msg.toString());
			}
			res.end();
		});
	});

	app.get('/appVmsg',function(req,res,next) {
		res.set('Content-Type', 'application/json; charset=utf-8');
		fs.readFile('./data/appVmsg.json','utf8',function(err,data) {
			if(err) {
        console.log(err);
        res.status(500).end();
      }
			else {
        res.end(data);
      }
		});
	});


	app.get('/getunivlist',function(req,res,next) {//�б� ����Ʈ
		univR.getunivlist(function(msg) {
			if(typeof(msg) === 'string') {
				res.set('Content-Type', 'text/plain');
				res.status(500).send(msg);
			} else {
				res.set('Content-Type', 'application/json; charset=utf-8');
				res.send(msg);
			}
			res.end();
		});
	});

	app.get('/feedback',function(req,res,nect) {
		res.set('Content-Type', 'text/plain');
		clientR.feedback(req.query.contents,req.query.os,req.query.ver,function(msg) {
			if(typeof(msg) == 'err') {
				res.status(500).send(msg);
			} else {
				res.send(msg);
			}
			res.end();
		});
	});

	app.all('*',function(req,res,next){
		res.sendStatus(404);
		res.end();
	});


} , {count:3});


function log(wid,msg){
	util.slog(wid,msg);
}

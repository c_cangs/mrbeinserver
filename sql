-- 내 스키마
CREATE SCHEMA `001`;

-- building
CREATE TABLE `001`.`building` (
	`bname`  VARCHAR(50) NOT NULL, -- bname
	`lati`   DOUBLE      NULL,     -- lati
	`longti` DOUBLE      NULL      -- longti
);

-- building
ALTER TABLE `001`.`building`
	ADD CONSTRAINT `PK_building` -- building 기본키
		PRIMARY KEY (
			`bname` -- bname
		);

-- class
CREATE TABLE `001`.`class` (
	`cid`        VARCHAR(100) NOT NULL, -- cid
	`year`       INT          NOT NULL, -- year
	`term`       INT          NOT NULL, -- term
	`department` VARCHAR(50)  NULL,     -- department
	`point`      INT          NULL,     -- point
	`type`       INT          NULL,     -- type
	`cname`      VARCHAR(50)  NOT NULL, -- cname
	`prof`       VARCHAR(50)  NULL,     -- prof
	`grade`      INT          NULL      -- grade
);

-- class
ALTER TABLE `001`.`class`
	ADD CONSTRAINT `PK_class` -- class 기본키
		PRIMARY KEY (
			`cid`,  -- cid
			`year`, -- year
			`term`  -- term
		);

-- student
CREATE TABLE `001`.`student` (
	`email`       VARCHAR(255) NOT NULL, -- email
	`kind`        varchar(5)   NOT NULL, -- kind
	`department`  VARCHAR(50)  NULL,     -- department
	`ddepartment` VARCHAR(50)  NULL      -- ddepartment
);

-- student
ALTER TABLE `001`.`student`
	ADD CONSTRAINT `PK_student` -- student 기본키
		PRIMARY KEY (
			`email`, -- email
			`kind`   -- kind
		);

-- classroom
CREATE TABLE `001`.`classroom` (
	`bname` VARCHAR(50) NOT NULL, -- bname
	`rid`   VARCHAR(50) NOT NULL, -- rid
	`floor` INT         NULL      -- floor
);

-- classroom
ALTER TABLE `001`.`classroom`
	ADD CONSTRAINT `PK_classroom` -- classroom 기본키
		PRIMARY KEY (
			`bname`, -- bname
			`rid`    -- rid
		);

-- classtable
CREATE TABLE `001`.`classtable` (
	`bname`   VARCHAR(50)  NOT NULL, -- bname
	`rid`     VARCHAR(50)  NOT NULL, -- rid
	`cid`     VARCHAR(100) NOT NULL, -- cid
	`year`    INT          NOT NULL, -- year
	`term`    INT          NOT NULL, -- term
	`cday`    INT          NOT NULL, -- cday
	`speriod` INT          NULL,     -- speriod
	`eperiod` INT          NULL      -- eperiod
);

-- classtable
ALTER TABLE `001`.`classtable`
	ADD CONSTRAINT `PK_classtable` -- classtable 기본키
		PRIMARY KEY (
			`bname`, -- bname
			`rid`,   -- rid
			`cid`,   -- cid
			`year`,  -- year
			`term`,  -- term
			`cday`   -- cday
		);

-- rfavorite
CREATE TABLE `001`.`rfavorite` (
	`email` VARCHAR(255) NOT NULL, -- email
	`kind`  varchar(5)   NOT NULL, -- kind
	`bname` VARCHAR(50)  NOT NULL, -- bname
	`rid`   VARCHAR(50)  NOT NULL  -- rid
);

-- rfavorite
ALTER TABLE `001`.`rfavorite`
	ADD CONSTRAINT `PK_rfavorite` -- rfavorite 기본키
		PRIMARY KEY (
			`email`, -- email
			`kind`,  -- kind
			`bname`, -- bname
			`rid`    -- rid
		);

-- timetable
CREATE TABLE `001`.`schedule` (
	`period` INT  NOT NULL, -- period
	`stime`  VARCHAR(10) NULL, -- stime
    `etime`  VARCHAR(10) NULL -- etime
);

-- timetable
ALTER TABLE `001`.`schedule`
	ADD CONSTRAINT `PK_schedule` -- timetable 기본키
		PRIMARY KEY (
			`period` -- period
		);

-- etc
CREATE TABLE `001`.`etc` (
	`version` INT NOT NULL, -- version
	`year`    INT NULL,     -- year
	`term`    INT NULL      -- term
);

-- etc
ALTER TABLE `001`.`etc`
	ADD CONSTRAINT `PK_etc` -- etc 기본키
		PRIMARY KEY (
			`version` -- version
		);

-- stimetable
CREATE TABLE `001`.`stimetable` (
	`email` VARCHAR(255) NOT NULL, -- email
	`kind`  varchar(5)   NOT NULL, -- kind
	`num`   INT          NOT NULL, -- num
	`cid`   VARCHAR(100) NULL,     -- cid
	`year`  INT          NULL,     -- year
	`term`  INT          NULL,     -- term
	`name`  varchar(50)  NULL,     -- name
	`day`   INT          NULL,     -- day
	`start` INT          NULL,     -- start
	`end`   INT          NULL      -- end
);

-- stimetable
ALTER TABLE `001`.`stimetable`
	ADD CONSTRAINT `PK_stimetable` -- stimetable 기본키
		PRIMARY KEY (
			`email`, -- email
			`kind`,  -- kind
			`num`    -- num
		);

-- studypoint
CREATE TABLE `001`.`studypoint` (
	`email` VARCHAR(255) NOT NULL, -- email
	`kind`  varchar(5)   NOT NULL, -- kind
	`cid`   VARCHAR(100) NOT NULL, -- cid
	`year`  INT          NOT NULL, -- year
	`term`  INT          NOT NULL, -- term
	`point` DOUBLE       NOT NULL  -- point
);

-- studypoint
ALTER TABLE `001`.`studypoint`
	ADD CONSTRAINT `PK_studypoint` -- studypoint 기본키
		PRIMARY KEY (
			`email`, -- email
			`kind`,  -- kind
			`cid`,   -- cid
			`year`,  -- year
			`term`   -- term
		);

-- bfavorite
CREATE TABLE `001`.`bfavorite` (
	`email` VARCHAR(255) NOT NULL, -- email
	`kind`  varchar(5)   NOT NULL, -- kind
	`bname` VARCHAR(50)  NOT NULL  -- bname
);

-- bfavorite
ALTER TABLE `001`.`bfavorite`
	ADD CONSTRAINT `PK_bfavorite` -- bfavorite 기본키
		PRIMARY KEY (
			`email`, -- email
			`kind`,  -- kind
			`bname`  -- bname
		);

-- department
CREATE TABLE `001`.`department` (
	`department` VARCHAR(50) NOT NULL -- department
);

-- department
ALTER TABLE `001`.`department`
	ADD CONSTRAINT `PK_department` -- department 기본키
		PRIMARY KEY (
			`department` -- department
		);

-- class
ALTER TABLE `001`.`class`
	ADD CONSTRAINT `FK_department_TO_class` -- department -> class
		FOREIGN KEY (
			`department` -- department
		)
		REFERENCES `001`.`department` ( -- department
			`department` -- department
		)
		ON DELETE NO ACTION
		ON UPDATE CASCADE;

-- student
ALTER TABLE `001`.`student`
	ADD CONSTRAINT `FK_department_TO_student` -- department -> student
		FOREIGN KEY (
			`department` -- department
		)
		REFERENCES `001`.`department` ( -- department
			`department` -- department
		)
		ON DELETE NO ACTION
		ON UPDATE CASCADE;

-- student
ALTER TABLE `001`.`student`
	ADD CONSTRAINT `FK_department_TO_student2` -- department -> student2
		FOREIGN KEY (
			`ddepartment` -- ddepartment
		)
		REFERENCES `001`.`department` ( -- department
			`department` -- department
		)
		ON DELETE NO ACTION
		ON UPDATE CASCADE;

-- classroom
ALTER TABLE `001`.`classroom`
	ADD CONSTRAINT `FK_building_TO_classroom` -- building -> classroom
		FOREIGN KEY (
			`bname` -- bname
		)
		REFERENCES `001`.`building` ( -- building
			`bname` -- bname
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- classtable
ALTER TABLE `001`.`classtable`
	ADD CONSTRAINT `FK_class_TO_classtable` -- class -> classtable
		FOREIGN KEY (
			`cid`,  -- cid
			`year`, -- year
			`term`  -- term
		)
		REFERENCES `001`.`class` ( -- class
			`cid`,  -- cid
			`year`, -- year
			`term`  -- term
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- classtable
ALTER TABLE `001`.`classtable`
	ADD CONSTRAINT `FK_classroom_TO_classtable` -- classroom -> classtable
		FOREIGN KEY (
			`bname`, -- bname
			`rid`    -- rid
		)
		REFERENCES `001`.`classroom` ( -- classroom
			`bname`, -- bname
			`rid`    -- rid
		)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;

-- rfavorite
ALTER TABLE `001`.`rfavorite`
	ADD CONSTRAINT `FK_student_TO_rfavorite` -- student -> rfavorite
		FOREIGN KEY (
			`email`, -- email
			`kind`   -- kind
		)
		REFERENCES `001`.`student` ( -- student
			`email`, -- email
			`kind`   -- kind
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- rfavorite
ALTER TABLE `001`.`rfavorite`
	ADD CONSTRAINT `FK_classroom_TO_rfavorite` -- classroom -> rfavorite
		FOREIGN KEY (
			`bname`, -- bname
			`rid`    -- rid
		)
		REFERENCES `001`.`classroom` ( -- classroom
			`bname`, -- bname
			`rid`    -- rid
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- stimetable
ALTER TABLE `001`.`stimetable`
	ADD CONSTRAINT `FK_student_TO_stimetable` -- student -> stimetable
		FOREIGN KEY (
			`email`, -- email
			`kind`   -- kind
		)
		REFERENCES `001`.`student` ( -- student
			`email`, -- email
			`kind`   -- kind
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- studypoint
ALTER TABLE `001`.`studypoint`
	ADD CONSTRAINT `FK_student_TO_studypoint` -- student -> studypoint
		FOREIGN KEY (
			`email`, -- email
			`kind`   -- kind
		)
		REFERENCES `001`.`student` ( -- student
			`email`, -- email
			`kind`   -- kind
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- studypoint
ALTER TABLE `001`.`studypoint`
	ADD CONSTRAINT `FK_class_TO_studypoint` -- class -> studypoint
		FOREIGN KEY (
			`cid`,  -- cid
			`year`, -- year
			`term`  -- term
		)
		REFERENCES `001`.`class` ( -- class
			`cid`,  -- cid
			`year`, -- year
			`term`  -- term
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- bfavorite
ALTER TABLE `001`.`bfavorite`
	ADD CONSTRAINT `FK_building_TO_bfavorite` -- building -> bfavorite
		FOREIGN KEY (
			`bname` -- bname
		)
		REFERENCES `001`.`building` ( -- building
			`bname` -- bname
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;

-- bfavorite
ALTER TABLE `001`.`bfavorite`
	ADD CONSTRAINT `FK_student_TO_bfavorite` -- student -> bfavorite
		FOREIGN KEY (
			`email`, -- email
			`kind`   -- kind
		)
		REFERENCES `001`.`student` ( -- student
			`email`, -- email
			`kind`   -- kind
		)
		ON DELETE CASCADE
		ON UPDATE CASCADE;